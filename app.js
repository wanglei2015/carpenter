'use strict';
var domain = require('domain');
var express = require('express');
var path = require('path');
// var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var todos = require('./routes/todos');
var expressLayouts = require('express-ejs-layouts');
var cloud = require('./cloud');
var _ = require('underscore');
var AV = require('leanengine');
var app = express();

// app.use(AV.Cloud);
app.use(AV.Cloud.CookieSession({
  secret: 'zNTTFqrSCDqbYWBacAyyQX81',
  maxAge: 3600000,
  fetchUser: true
}));
// 设置 view 引擎
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static('public'));

// 加载云代码方法
app.use(cloud);
app.use(expressLayouts);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());

// 未处理异常捕获 middleware
app.use(function(req, res, next) {
  var d = domain.create();
  d.add(req);
  d.add(res);
  d.on('error', function(err) {
    console.error('uncaughtException url=%s, msg=%s', req.url, err.stack || err.message || err);
    if(!res.finished) {
      res.statusCode = 500;
      res.setHeader('content-type', 'application/json; charset=UTF-8');
      res.end('uncaughtException');
    }
  });
  d.run(next);
});


app.get('/', function(req, res) {
  res.redirect('/posts');
});

  
app.get('/login', function(req, res) {
  res.render('login.ejs');
})

app.get('/signup', function(req, res) {
  res.render('signup.ejs');
})


app.get('/posts', function (req, res) {
  // console.log(AV.User.current())
  var posts = [
    {name: '让浴室成为宠爱自己的空间', desc:'12312312312312', image: 'http://imgs-qn.iliangcang.com/ware/topic/cover/1/427.jpg'},
    {name: '像洗脸一样洗衣服', desc:'12312312312312', image: 'http://imgs-qn.iliangcang.com/ware/topic/cover/1/423.jpg'},
    {name: '给书虫的最美书签', desc:'12312312312312', image: 'http://imgs-qn.iliangcang.com/ware/topic/cover/1/415.jpg'}
  ]

  res.render('posts', {
              user: req.AV.user,
              title: 'post 列表',
              posts: posts
            });

});

app.get('/posts/:id', function (req, res) {
  var post = {name: '111', desc:'100'}
  // console.log(posts);

  res.render('post', {
              title: 'post 详情',
              post: post,
              user: req.AV.user
            });

});

app.get('/products', function (req, res) {
  var products = [
    {name: '轻松调频EZFM', price:'100', image: 'http://imgs-qn.iliangcang.com/ware/upload/orig/2/151/151102.jpg'},
    {name: 'ANTONE CREATIVE', price:'100', image: 'http://imgs-qn.iliangcang.com/ware/upload/orig/2/317/317842.jpg'},
    {name: '轻松调频EZFM', price:'100', image: 'http://imgs-qn.iliangcang.com/ware/upload/orig/2/317/317844.jpg'},
    {name: 'ANTONE CREATIVE', price:'100', image: 'http://imgs-qn.iliangcang.com/ware/upload/orig/2/285/285229.jpg'},
    {name: '几何形书页夹', price:'200', image: 'http://imgs-qn.iliangcang.com/ware/upload/orig/2/308/308488.jpg'},
    {name: '黄铜书签(手指)', price:'200', image: 'http://imgs-qn.iliangcang.com/ware/upload/orig/2/151/151836.jpg'},
    {name: '宇航员 被套+枕套 套装（单/双人）', price:'200', image: 'http://imgs-qn.iliangcang.com/ware/upload/orig/2/279/279283.jpg'}
  ]
  // console.log(products);

  res.render('products', {
              title: 'product 列表',
              products: products,
              user: req.AV.user
            });

});

app.get('/products/:id', function (req, res) {
  var product = {name: '111', price:'100', image: 'http://imgs-qn.iliangcang.com/ware/goods/big/2/232/232243.jpg'}
  // console.log(products);

  res.render('product', {
              title: 'product 详情',
              product: product,
              user: req.AV.user
            });
});


// 可以将一类的路由单独保存在一个文件中
app.use('/todos', todos);


// 如果任何路由都没匹配到，则认为 404
// 生成一个异常让后面的 err handler 捕获
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// 如果是开发环境，则将异常堆栈输出到页面，方便开发调试
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) { // jshint ignore:line
    var statusCode = err.status || 500;
    if(statusCode === 500) {
      console.error(err.stack || err);
    }
    res.status(statusCode);
    res.render('error', {
      message: err.message || err,
      error: err
    });
  });
}

// 如果是非开发环境，则页面只输出简单的错误信息
app.use(function(err, req, res, next) { // jshint ignore:line
  res.status(err.status || 500);
  res.render('error', {
    message: err.message || err,
    error: {}
  });
});

module.exports = app;
